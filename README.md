# Project fullstack graphql with NodeJS - TypeScript

## Technologies

- React
- TypeScript
- GraphQL
- URQL/Apollo
- Node.js
- PostgreSQL
- TypeORM
- Redis
- Next.js
- TypeGraphQL
- Chakra

Some images of projects:

<div align="center">
  <img src="docs/img/home-page.png"/>
  <img src="docs/img/login-page.png"/>
  <img src="docs/img/register-page.png"/>
  <img src="docs/img/posts-page.png"/>
  <img src="docs/img/post-detail-page.png"/>
  <img src="docs/img/home-mobile.png"/>
</div>