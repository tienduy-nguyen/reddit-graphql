export function addServerErrors(
  errors: Record<string, string>,
  setError: (
    fieldName: string,
    error: { type: string; message: string }
  ) => void
) {
  return Object.keys(errors).forEach((key) => {
    setError(key, {
      type: 'server',
      message: errors[key],
    });
  });
}
