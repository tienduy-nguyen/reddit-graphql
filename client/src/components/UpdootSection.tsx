import { ChevronDownIcon, ChevronUpIcon } from '@chakra-ui/icons';
import { Flex, useToast } from '@chakra-ui/react';
import React, { useState } from 'react';
import {
  PostSnippetFragment,
  useVoteMutation,
  VoteMutation,
} from '../generated/graphql';
import gql from 'graphql-tag';
import { ApolloCache } from '@apollo/client';
import { useIsAuth } from 'src/hooks/useIsAuth';
import { NextRouter, useRouter } from 'next/router';
import { useMeQueryHook } from 'src/hooks/useMeQueryHook';
interface UpdootSectionProps {
  post: PostSnippetFragment;
}
const updateAfterVote = (
  value: number,
  postId: string,
  cache: ApolloCache<VoteMutation>
) => {
  const data = cache.readFragment<{
    id: string;
    points: number;
    voteStatus: number | null;
  }>({
    id: 'Post:' + postId,
    fragment: gql`
      fragment _ on Post {
        id
        points
        voteStatus
      }
    `,
  });

  if (data) {
    if (data.voteStatus === value) {
      return;
    }
    const newPoints =
      (data.points as number) + (!data.voteStatus ? 1 : 2) * value;
    cache.writeFragment({
      id: 'Post:' + postId,
      fragment: gql`
        fragment __ on Post {
          points
          voteStatus
        }
      `,
      data: { points: newPoints, voteStatus: value },
    });
  }
};

const noPermissionAlert = (toast: any) => {
  toast({
    title: 'Vote',
    description: 'You need to login to take this action!',
    status: 'warning',
    duration: 2000,
    isClosable: true,
    position: 'top-right',
  });
};

export const UpdootSection: React.FC<UpdootSectionProps> = ({ post }) => {
  const toast = useToast();
  const [, setLoadingState] = useState<
    'updoot-loading' | 'downdoot-loading' | 'not-loading'
  >('not-loading');
  const [vote] = useVoteMutation();
  const [user] = useMeQueryHook();

  return (
    <Flex direction='column' justifyContent='center' alignItems='center' mr={4}>
      <ChevronUpIcon
        cursor='pointer'
        onClick={async () => {
          if (!user) {
            noPermissionAlert(toast);
            return;
          }
          if (post.voteStatus === 1) {
            return;
          }
          setLoadingState('updoot-loading');
          await vote({
            variables: {
              postId: post.id,
              value: 1,
            },
            update: (cache) => updateAfterVote(1, post.id, cache),
          });
          setLoadingState('not-loading');
        }}
        boxSize={6}
        color={post.voteStatus === 1 ? 'green' : undefined}
      />

      {post.points}
      <ChevronDownIcon
        onClick={async () => {
          if (!user) {
            noPermissionAlert(toast);
            return;
          }
          if (post.voteStatus === -1) {
            return;
          }
          setLoadingState('downdoot-loading');
          await vote({
            variables: {
              postId: post.id,
              value: -1,
            },
            update: (cache) => updateAfterVote(-1, post.id, cache),
          });
          setLoadingState('not-loading');
        }}
        cursor='pointer'
        boxSize={6}
        color={post.voteStatus === -1 ? 'red' : undefined}
      />
    </Flex>
  );
};
