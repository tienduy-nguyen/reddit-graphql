import Head from 'next/head';

export function MetaTags({
  title = 'Reddit GraphQL',
  description = 'Build Reddit Graphql clone with NodeJS + NextJS + GraphQL',
  keywords = 'nodejs, graphql, nextjs',
}) {
  return (
    <Head>
      <title>{title === 'Reddit GraphQL' ? title : `${title} | RG`}</title>
      <meta name='viewport' content='width=device-width, initial-scale=1' />
      <meta name='keywords' content={keywords} />
      <meta name='description' content={description} />
      <meta charSet='utf-8' />
      <link rel='icon' href='/favicon.ico' />
      <meta name='twitter:card' content='summary' />
      <meta name='twitter:site' content='@tienduy_nguyen' />
      <meta name='twitter:title' content={title} />
      <meta name='twitter:description' content={description} />

      <meta property='og:title' content={title} />
      <meta property='og:description' content={description} />
    </Head>
  );
}
