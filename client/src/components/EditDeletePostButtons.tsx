import React from 'react';
import { useDeletePostMutation, useMeQuery } from '../generated/graphql';
import { Box, Flex } from '@chakra-ui/react';
import { DeleteIcon, EditIcon } from '@chakra-ui/icons';
import router from 'next/router';

interface EditDeletePostButtonsProps {
  id: string;
  authorId: string;
}

export const EditDeletePostButtons: React.FC<EditDeletePostButtonsProps> = ({
  id,
  authorId,
}) => {
  const { data: meData } = useMeQuery();
  const [deletePost] = useDeletePostMutation();

  if (meData?.me?.id !== authorId) {
    return null;
  }

  return (
    <Box mb='4'>
      <Flex flexDirection='column'>
        <EditIcon
          mt='4'
          ml='4'
          boxSize={4}
          cursor='pointer'
          color='green'
          onClick={() => {
            router.push(`/post/edit/${id}`);
          }}
        />

        <DeleteIcon
          mt='4'
          ml='4'
          boxSize={4}
          cursor='pointer'
          color='red'
          onClick={() => {
            deletePost({
              variables: { id },
              update: (cache) => {
                cache.evict({ id: 'Post:' + id });
              },
            });
          }}
        />
      </Flex>
    </Box>
  );
};
