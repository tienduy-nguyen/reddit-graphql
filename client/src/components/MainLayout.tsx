import { Box } from '@chakra-ui/react';
import React from 'react';
import { Navbar } from 'src/components/Navbar';

interface LayoutProps {}

export const MainLayout: React.FC<LayoutProps> = ({ children }) => {
  return (
    <>
      <Navbar />
      <Box>{children}</Box>
    </>
  );
};
