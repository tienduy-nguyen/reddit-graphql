import React from 'react';
import { Box, Flex } from '@chakra-ui/react';

interface BoxWrapperProps {}

export const BoxWrapper: React.FC<BoxWrapperProps> = ({
  children,
  ...rest
}) => {
  return (
    <Flex justify='center' w='100%' align='center' mx='auto' {...rest}>
      <Box mt={4} mx='auto'>
        {children}
      </Box>
    </Flex>
  );
};
