import { Flex, Heading } from '@chakra-ui/react';

export const Hero = ({ title }: { title: string }) => (
  <Flex mt='2rem' ml='0'>
    <Heading fontSize='2em'>{title}</Heading>
  </Flex>
);

Hero.defaultProps = {
  title: 'Reddit GraphQL',
};
