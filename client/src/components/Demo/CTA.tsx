import { Link as ChakraLink, Button } from '@chakra-ui/react';

import { ContainerFluidWrapper } from '../ContainerFluidWrapper';

export const CTA = () => (
  <ContainerFluidWrapper
    flexDirection='row'
    position='fixed'
    bottom='0'
    width='100%'
    maxWidth='48rem'
    py={2}
    mx='auto'
  >
    <ChakraLink isExternal href='http://adev42.com' flexGrow={1} mx={2}>
      <Button width='100%' variant='outline' colorScheme='green'>
        Check another projects
      </Button>
    </ChakraLink>

    <ChakraLink
      isExternal
      href='https://github.com/tienduy-nguyen/reddit-clone'
      flexGrow={3}
      mx={2}
    >
      <Button width='100%' variant='solid' colorScheme='green'>
        View Github Repo
      </Button>
    </ChakraLink>
  </ContainerFluidWrapper>
);
