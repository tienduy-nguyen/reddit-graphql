import { Box, Button, Stack, Text, useToast } from '@chakra-ui/react';
import React from 'react';
import NextLink from 'next/link';
import { DarkModeSwitch } from './DarkModeSwitch';
import { useLogoutMutation, useMeQuery } from 'src/generated/graphql';
import router from 'next/router';
import { useApolloClient } from '@apollo/client';

interface MenuLinksProps {
  isOpen: boolean;
}

export const MenuLinks: React.FC<MenuLinksProps> = ({ isOpen }) => {
  const apolloClient = useApolloClient();
  const [logout, { loading: logoutFetching }] = useLogoutMutation();
  const { data, loading } = useMeQuery();

  let groupAuthButtons = (
    <>
      <MenuItem to='/login'>
        <Button size='sm' rounded='md' colorScheme='green' mr={2}>
          Login
        </Button>
      </MenuItem>
      <MenuItem to='/register'>
        <Button size='sm' rounded='md' colorScheme='green'>
          Register
        </Button>
      </MenuItem>
    </>
  );
  const toast = useToast();
  const logoutUser = async () => {
    await logout();
    await apolloClient.resetStore();
    toast({
      title: 'You are logged out.',
      description: 'Thank you. Hope you comeback soon!',
      status: 'info',
      duration: 1000,
      isClosable: true,
      position: 'bottom-right',
    });
    router.push('/');
  };

  // data is loading
  if (loading) {
  } else if (!loading && data?.me) {
    // user id logged in
    groupAuthButtons = (
      <>
        <MenuItem to='/profile'>
          <Button size='sm' rounded='md' colorScheme='green' mr={2}>
            Hi {data.me.username}
          </Button>
        </MenuItem>

        <Button
          size='sm'
          rounded='md'
          colorScheme='green'
          variant='outline'
          isLoading={logoutFetching}
          onClick={() => {
            logoutUser();
          }}
        >
          Logout
        </Button>
      </>
    );
  }

  return (
    <Box
      display={{ base: isOpen ? 'block' : 'none', md: 'block' }}
      flexBasis={{ base: '100%', md: 'auto' }}
    >
      <Stack
        spacing={6}
        align='center'
        justify={['center', 'space-between', 'flex-end', 'flex-end']}
        direction={['column', 'row', 'row', 'row']}
        pt={[2, 2, 0, 0]}
      >
        <DarkModeSwitch />
        <MenuItem to='/posts'>Posts</MenuItem>
        <MenuItem to='/posts/new'>Create Post</MenuItem>
        {groupAuthButtons}
      </Stack>
    </Box>
  );
};

const MenuItem = ({ children, to = '/', ...rest }) => {
  return (
    <NextLink href={to}>
      <Text display='block' {...rest} cursor='pointer'>
        {children}
      </Text>
    </NextLink>
  );
};
