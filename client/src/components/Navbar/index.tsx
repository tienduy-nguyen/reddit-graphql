import { Flex } from '@chakra-ui/react';
import React from 'react';
import { Logo } from './Logo';
import { MenuLinks } from './MenuLinks';
import { MenuToggle } from './MenuToggle';

interface NavbarProps {}

export const Navbar: React.FC<NavbarProps> = ({ ...props }) => {
  const [isOpen, setIsOpen] = React.useState(false);

  const toggle = () => setIsOpen(!isOpen);
  return (
    <Flex
      as='nav'
      align='center'
      justify='space-between'
      wrap='wrap'
      w='100%'
      p={6}
      bg={['primary.500', 'primary.500', 'transparent', 'transparent']}
      color={['white', 'white', 'primary.700', 'primary.700']}
      shadow='sm'
      {...props}
    >
      <>
        <Logo
          w='200px'
          color={['white', 'white', 'primary.500', 'primary.500']}
        />
        <MenuToggle toggle={toggle} isOpen={isOpen} />
        <MenuLinks isOpen={isOpen} />
      </>
    </Flex>
  );
};
