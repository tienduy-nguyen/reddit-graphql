import React from 'react';
import { Box, Link, Text } from '@chakra-ui/react';
import NextLink from 'next/link';

export const Logo = (props: any) => {
  return (
    <Box {...props}>
      <Link as={NextLink} href='/' fontSize='lg' fontWeight='bold'>
        <Text display='block' cursor='pointer'>
          Reddit GraphQL
        </Text>
      </Link>
    </Box>
  );
};
