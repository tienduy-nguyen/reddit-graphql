import { FormControl, FormLabel } from '@chakra-ui/form-control';
import * as yup from 'yup';
import { yupResolver } from '@hookform/resolvers/yup';
import { useForm } from 'react-hook-form';
import { Input, InputGroup, InputRightElement } from '@chakra-ui/input';
import React from 'react';
import {
  Button,
  FormErrorMessage,
  Link,
  Text,
  useToast,
} from '@chakra-ui/react';
import {
  MeDocument,
  MeQuery,
  useLoginMutation,
  useMeQuery,
} from 'src/generated/graphql';
import { useRouter } from 'next/router';
import { toErrorMap } from 'src/utils/toErrorMap';
import { addServerErrors } from 'src/utils/addServerError';
import { BoxWrapper } from 'src/components/BoxWrapper';
import { MetaTags } from 'src/components/MetaTags';
import { ViewIcon, ViewOffIcon } from '@chakra-ui/icons';
import NextLink from 'next/link';
import { NextPage } from 'next';
import { MainLayout } from 'src/components/MainLayout';

// Validation input form
const schema = yup.object().shape({
  username: yup.string().required(),
  password: yup.string().required(),
});

// Input for register form
type LoginInputForm = {
  username: string;
  password: string;
};

const LoginPage: NextPage = ({}) => {
  const router = useRouter();
  const toast = useToast();
  const [show, setShow] = React.useState(false);
  const handleClick = () => setShow(!show);

  const { data, loading } = useMeQuery();
  if (!loading && data?.me) {
    if (typeof router.query.next === 'string') {
      router.push(router.query.next);
    } else {
      router.push('/');
    }
  }
  const { register, handleSubmit, errors, setError } = useForm<LoginInputForm>({
    mode: 'onBlur',
    resolver: yupResolver(schema),
  });

  // Get mutation graphql from mutation string
  const [login] = useLoginMutation();

  // Submit form
  const onSubmit = async (values: LoginInputForm) => {
    const { username, password } = values;
    const response = await login({
      variables: { usernameOrEmail: username, password },
      update: (cache, { data }) => {
        cache.writeQuery<MeQuery>({
          query: MeDocument,
          data: {
            __typename: 'Query',
            me: data?.login.user,
          },
        });
        cache.evict({ fieldName: 'posts:{}' });
      },
    });
    console.log(response);
    if (response?.data?.login.errors) {
      const serverErrors: Record<string, string> = toErrorMap(
        response?.data?.login.errors
      );
      addServerErrors(serverErrors, setError);
    } else if (response?.data?.login.user) {
      // Alert
      toast({
        title: 'Logged in successfully!',
        status: 'success',
        duration: 2000,
        isClosable: true,
        position: 'top-right',
      });
      if (typeof router.query.next === 'string') {
        router.push(router.query.next);
      } else {
        router.push('/');
      }
    }
  };

  return (
    <MainLayout>
      <MetaTags title='Login' />
      <BoxWrapper>
        <form
          style={{
            width: 400,
          }}
        >
          <Text fontSize='2em' textAlign='center'>
            Login
          </Text>
          <FormControl
            isInvalid={!!errors?.username?.message}
            errortext={errors?.username?.message}
            isRequired
            p='4'
            mt='4'
          >
            <FormLabel>Username</FormLabel>
            <Input
              type='text'
              name='username'
              placeholder='username'
              id='username'
              autoComplete='username'
              ref={register}
            />
            <FormErrorMessage>{errors?.username?.message}</FormErrorMessage>
          </FormControl>

          <FormControl
            isInvalid={!!errors?.password?.message}
            errortext={errors?.password?.message}
            px='4'
            pb='4'
            isRequired
          >
            <FormLabel>Password</FormLabel>
            <InputGroup>
              <Input
                type={show ? 'text' : 'password'}
                name='password'
                placeholder='Password'
                id='password'
                autoComplete='current-password'
                ref={register}
              />
              <InputRightElement width='4.5rem'>
                <Button h='1.75rem' size='sm' onClick={handleClick}>
                  {show ? <ViewOffIcon /> : <ViewIcon />}
                </Button>
              </InputRightElement>
            </InputGroup>
            <FormErrorMessage>{errors?.password?.message}</FormErrorMessage>
          </FormControl>

          <NextLink href='/forgot-password'>
            <Link
              style={{
                display: 'block',
                fontSize: '0.8rem',
                width: '10rem',
                marginLeft: 'auto',
                marginRight: '0rem',
              }}
            >
              Forgot password?
            </Link>
          </NextLink>

          <Button
            mb='15'
            onClick={handleSubmit(onSubmit)}
            p='4'
            mx='4'
            mt='6'
            w='90%'
            colorScheme='green'
            type='submit'
          >
            Login
          </Button>
        </form>
      </BoxWrapper>
    </MainLayout>
  );
};

// export default withUrqlClient(createUrqlClient)(Login);
export default LoginPage;
// export default withApollo({ ssr: false })(LoginPage);
