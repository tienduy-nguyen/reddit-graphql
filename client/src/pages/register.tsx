import { FormControl, FormLabel } from '@chakra-ui/form-control';
import * as yup from 'yup';
import { yupResolver } from '@hookform/resolvers/yup';
import { useForm } from 'react-hook-form';
import { Input } from '@chakra-ui/input';
import React from 'react';
import { Button, FormErrorMessage, Text, useToast } from '@chakra-ui/react';
import {
  MeDocument,
  MeQuery,
  useMeQuery,
  useRegisterMutation,
} from 'src/generated/graphql';
import { useRouter } from 'next/router';
import { toErrorMap } from 'src/utils/toErrorMap';
import { addServerErrors } from 'src/utils/addServerError';
import { BoxWrapper } from 'src/components/BoxWrapper';
import { MetaTags } from 'src/components/MetaTags';
import { MainLayout } from 'src/components/MainLayout';

interface registerProps {}

// Validation input form
const schema = yup.object().shape({
  username: yup.string().required(),
  email: yup.string().email().required(),
  password: yup.string().min(3).required(),
  password_confirmation: yup
    .string()
    .required()
    .oneOf([yup.ref('password'), null], 'Passwords must match!'),
});

// Input for register form
type RegisterFormInputs = {
  username: string;
  email: string;
  password: string;
  password_confirmation: string;
};

const RegisterPage: React.FC<registerProps> = ({}) => {
  const router = useRouter();
  const toast = useToast();
  const { data, loading } = useMeQuery();
  if (!loading && data?.me) {
    router.push('/');
    toast({
      title: 'Registration warning.',
      description:
        'You need to logout first, if you want to create a new account!',
      status: 'warning',
      duration: 3000,
      isClosable: true,
      position: 'bottom-right',
    });
    router.push('/');
  }

  const {
    register,
    handleSubmit,
    errors,
    setError,
  } = useForm<RegisterFormInputs>({
    mode: 'onBlur',
    resolver: yupResolver(schema),
  });

  // Get mutation graphql from mutation string
  const [registerUser] = useRegisterMutation();

  // Submit form
  const onSubmit = async (values: RegisterFormInputs) => {
    const { username, email, password } = values;
    const response = await registerUser({
      variables: { username, email, password },
      update: (cache, { data }) => {
        cache.writeQuery<MeQuery>({
          query: MeDocument,
          data: {
            __typename: 'Query',
            me: data?.register.user,
          },
        });
      },
    });
    if (response?.data?.register.errors) {
      const serverErrors: Record<string, string> = toErrorMap(
        response?.data?.register.errors
      );
      addServerErrors(serverErrors, setError);
    } else if (response?.data?.register.user) {
      // worked
      toast({
        title: 'Account created.',
        status: 'success',
        duration: 2000,
        isClosable: true,
        position: 'top-right',
      });
      router.push('/');
    }
  };

  return (
    <MainLayout>
      <MetaTags title='Register' />
      <BoxWrapper>
        <form
          style={{
            width: 400,
          }}
        >
          <Text fontSize='2em' textAlign='center'>
            Registration account
          </Text>
          <FormControl
            isInvalid={!!errors?.username?.message}
            errortext={errors?.username?.message}
            isRequired
            p='4'
            mt='4'
          >
            <FormLabel>Username</FormLabel>
            <Input
              type='text'
              name='username'
              placeholder='username'
              id='username'
              autoComplete='username'
              ref={register}
            />
            <FormErrorMessage>{errors?.username?.message}</FormErrorMessage>
          </FormControl>
          <FormControl
            isInvalid={!!errors?.email?.message}
            errortext={errors?.email?.message}
            isRequired
            p='4'
          >
            <FormLabel>Email</FormLabel>
            <Input
              type='email'
              name='email'
              placeholder='email'
              id='email'
              autoComplete='email'
              ref={register}
            />
            <FormErrorMessage>{errors?.email?.message}</FormErrorMessage>
          </FormControl>

          <FormControl
            isInvalid={!!errors?.password?.message}
            errortext={errors?.password?.message}
            px='4'
            pb='4'
            isRequired
          >
            <FormLabel>Password</FormLabel>
            <Input
              type='password'
              name='password'
              placeholder='Password'
              id='password'
              autoComplete='current-password'
              ref={register}
            />
            <FormErrorMessage>{errors?.password?.message}</FormErrorMessage>
          </FormControl>
          <FormControl
            isInvalid={!!errors?.password_confirmation?.message}
            errortext={errors?.password_confirmation?.message}
            px='4'
            pb='4'
            isRequired
          >
            <FormLabel>Password confirmation</FormLabel>
            <Input
              type='password'
              name='password_confirmation'
              placeholder='Password confirmation'
              id='password_confirmation'
              autoComplete='password_confirmation'
              ref={register}
            />
            <FormErrorMessage>
              {errors?.password_confirmation?.message}
            </FormErrorMessage>
          </FormControl>
          <Button
            mb='15'
            onClick={handleSubmit(onSubmit)}
            p='4'
            mx='4'
            mt='6'
            w='90%'
            colorScheme='green'
          >
            Sign Up
          </Button>
        </form>
      </BoxWrapper>
    </MainLayout>
  );
};

// export default withUrqlClient(createUrqlClient)(Register);
export default RegisterPage;
// export default withApollo({ ssr: false })(RegisterPage);
