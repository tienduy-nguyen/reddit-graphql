import {
  Box,
  Text,
  Flex,
  Heading,
  Spinner,
  Stack,
  Button,
  Link,
} from '@chakra-ui/react';
import { NextPage } from 'next';
import React, { useState } from 'react';
import { ContainerFluidWrapper } from 'src/components/ContainerFluidWrapper';
import { MetaTags } from 'src/components/MetaTags';
import { UpdootSection } from 'src/components/UpdootSection';
import { usePostsQuery } from 'src/generated/graphql';
import NextLink from 'next/link';
import { EditDeletePostButtons } from 'src/components/EditDeletePostButtons';
import { useMeQueryHook } from 'src/hooks/useMeQueryHook';
import { MainLayout } from 'src/components/MainLayout';

const PostsPage: NextPage = () => {
  const [variableInput, setVariableInput] = useState({
    limit: 20,
    offset: 0,
  });
  const [user] = useMeQueryHook();

  const { data, error, loading, variables } = usePostsQuery({
    variables: { ...variableInput },
  });

  if (!loading && !data) {
    return (
      <ContainerFluidWrapper>
        <Flex align='center' flexDirection='column' height='100vh' p='4'>
          <Heading>You got query failed for some reason</Heading>
          <Text mt='4'>{error?.message}</Text>
        </Flex>
      </ContainerFluidWrapper>
    );
  }

  return (
    <MainLayout>
      <MetaTags title='Posts' />
      <ContainerFluidWrapper w='100%' mb='4rem' minW='80rem'>
        <Flex align='center' mt='5'>
          <Heading>Posts</Heading>
        </Flex>

        <br />
        {!data && loading ? (
          <Spinner
            thickness='4px'
            speed='0.65s'
            emptyColor='gray.200'
            colorScheme='green'
            size='sm'
          />
        ) : (
          <>
            <Stack spacing={8} minW='64rem'>
              {data!.posts.posts.map((p) =>
                !p ? null : (
                  <Flex key={p.id} p={5} shadow='md' borderWidth='1px'>
                    <UpdootSection post={p} />
                    <Box flex={1}>
                      <Flex>
                        <Box>
                          <NextLink href='/post/[id]' as={`/post/${p.id}`}>
                            <Link>
                              <Heading fontSize='xl'>{p.title}</Heading>
                            </Link>
                          </NextLink>
                          <Text>posted by {p.author.username}</Text>
                        </Box>
                        {p.author.id === user?.id ? (
                          <Box ml='auto'>
                            <EditDeletePostButtons
                              id={p.id}
                              authorId={p.author.id}
                            />
                          </Box>
                        ) : null}
                      </Flex>

                      <Text flex={1} mt={4}>
                        {p.contentSnippet}
                      </Text>
                    </Box>
                  </Flex>
                )
              )}
            </Stack>
          </>
        )}

        {data && data.posts.hasMore ? (
          <Flex>
            <Button
              onClick={() => {
                setVariableInput({
                  limit: variables.limit,
                  offset: variables.offset + variables.limit,
                });
              }}
              m='auto'
              my={8}
            >
              load more
            </Button>
          </Flex>
        ) : null}
      </ContainerFluidWrapper>
    </MainLayout>
  );
};

export default PostsPage;
// export default withApollo({ ssr: true })(PostsPage);
