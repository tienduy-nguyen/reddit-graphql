import { FormControl, FormLabel } from '@chakra-ui/form-control';
import * as yup from 'yup';
import { yupResolver } from '@hookform/resolvers/yup';
import { useForm } from 'react-hook-form';
import { Input, InputGroup, InputRightElement } from '@chakra-ui/input';
import React from 'react';
import {
  Button,
  FormErrorMessage,
  Link,
  Text,
  Textarea,
  useToast,
} from '@chakra-ui/react';
import { useCreatePostMutation } from 'src/generated/graphql';
import { useRouter } from 'next/router';
import { MetaTags } from 'src/components/MetaTags';
import { NextPage } from 'next';
import { useIsAuth } from 'src/hooks/useIsAuth';
import { useMeQueryHook } from 'src/hooks/useMeQueryHook';
import { withApollo } from 'src/utils/withApollo';
import { MainLayout } from 'src/components/MainLayout';

// Validation input form
const schema = yup.object().shape({
  title: yup.string().required(),
  content: yup.string().min(10).required(),
});

// Input for register form
type CreatePostInputForm = {
  title: string;
  content: string;
};

const CreatePostPage: NextPage = ({}) => {
  const router = useRouter();
  const toast = useToast();
  useIsAuth();

  const {
    register,
    handleSubmit,
    errors,
    setError,
  } = useForm<CreatePostInputForm>({
    mode: 'onBlur',
    resolver: yupResolver(schema),
  });

  // Get mutation graphql from mutation string
  const [createPost] = useCreatePostMutation();

  // Submit form
  const onSubmit = async (values: CreatePostInputForm) => {
    const response = await createPost({
      variables: {
        data: {
          title: values.title,
          content: values.content,
        },
      },
    });

    if (response?.data?.createPost) {
      // Alert
      toast({
        title: 'Create post successfully!',
        status: 'success',
        duration: 1000,
        isClosable: true,
        position: 'top-right',
      });
      router.push('/posts');
    }
  };

  return (
    <MainLayout>
      <MetaTags title='Create Post' />
      <form
        style={{
          paddingLeft: '4rem',
          paddingRight: '4rem',
          maxWidth: '80rem',
          marginLeft: 'auto',
          marginRight: 'auto',
        }}
      >
        <Text fontSize='2em' textAlign='center'>
          Create post
        </Text>

        <FormControl
          isInvalid={!!errors?.title?.message}
          errortext={errors?.title?.message}
          isRequired
          p='4'
          mt='4'
        >
          <FormLabel>Title</FormLabel>
          <Input
            type='text'
            name='title'
            id='title'
            placeholder='Title of post'
            autoComplete='title'
            ref={register}
          />
          <FormErrorMessage>{errors?.title?.message}</FormErrorMessage>
        </FormControl>

        <FormControl
          isInvalid={!!errors?.content?.message}
          errortext={errors?.content?.message}
          px='4'
          pb='4'
          isRequired
        >
          <FormLabel>Content</FormLabel>
          <Textarea
            type='input'
            placeholder='Content of post'
            name='content'
            id='content'
            rows={18}
            ref={register}
          ></Textarea>

          <FormErrorMessage>{errors?.content?.message}</FormErrorMessage>
        </FormControl>

        <Button
          mb='15'
          onClick={handleSubmit(onSubmit)}
          p='4'
          mx='4'
          mt='6'
          w='6rem'
          colorScheme='green'
          type='submit'
        >
          Submit
        </Button>
      </form>
    </MainLayout>
  );
};

// export default withUrqlClient(createUrqlClient)(Login);
export default CreatePostPage;
// export default withApollo({ ssr: false })(CreatePostPage);
