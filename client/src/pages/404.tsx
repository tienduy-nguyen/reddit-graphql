import React from 'react';
import NextLink from 'next/link';
import { Button } from '@chakra-ui/react';
import { Text } from '@chakra-ui/layout';
import { ContainerFluidWrapper } from 'src/components/ContainerFluidWrapper';
import { BoxWrapper } from 'src/components/BoxWrapper';
import Image from 'next/image';
import { MetaTags } from 'src/components/MetaTags';
import { MainLayout } from 'src/components/MainLayout';

const Custom404 = () => (
  <MainLayout>
    <MetaTags title='Not Found' />
    <ContainerFluidWrapper height='100vh'>
      <BoxWrapper>
        <Text mb='4' fontSize='2xl' mx='auto' display='block'>
          404 - That page does not seem to exist...
        </Text>
        <Image src='/giphy.gif' width='480' height='362'></Image>
        <NextLink href='/'>
          <Button colorScheme='green' mt={6} mx='auto' display='block'>
            Go home
          </Button>
        </NextLink>
      </BoxWrapper>
    </ContainerFluidWrapper>
  </MainLayout>
);

export default Custom404;
// export default withApollo({ ssr: false })(Custom404);
