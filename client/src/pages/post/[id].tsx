import { Box, Button, Flex, Heading, Spinner } from '@chakra-ui/react';
import { NextPage } from 'next';
import { useRouter } from 'next/router';
import React from 'react';
import { ContainerFluidWrapper } from 'src/components/ContainerFluidWrapper';
import { EditDeletePostButtons } from 'src/components/EditDeletePostButtons';
import { MainLayout } from 'src/components/MainLayout';
import { useGetPostFromUrl } from 'src/hooks/useGetPostFromUrl';
import { withApollo } from 'src/utils/withApollo';

const PostDetailPage: NextPage = ({}) => {
  const { data, error, loading } = useGetPostFromUrl();
  const router = useRouter();
  if (loading) {
    return (
      <Spinner
        thickness='4px'
        speed='0.65s'
        emptyColor='gray.200'
        colorScheme='green'
        size='sm'
      />
    );
  }

  if (error) {
    return <div>{error.message}</div>;
  }

  if (!data?.post) {
    return (
      <ContainerFluidWrapper>
        <Box>could not find post</Box>
      </ContainerFluidWrapper>
    );
  }

  return (
    <MainLayout>
      <ContainerFluidWrapper minH='100vh' p={6}>
        <Flex maxW='64rem'>
          <Box>
            <Heading mb={4}>{data.post.title}</Heading>
            <Box mb={4}>{data.post.content}</Box>
          </Box>
          <Box flex={1}>
            <EditDeletePostButtons
              id={data.post.id}
              authorId={data.post.author.id}
            />
            <Button
              colorScheme='green'
              p='2'
              ml='4'
              onClick={() => router.back()}
              size='sm'
            >
              Back
            </Button>
          </Box>
        </Flex>
      </ContainerFluidWrapper>
    </MainLayout>
  );
};

export default PostDetailPage;
// export default withApollo({ ssr: true })(PostDetailPage);
