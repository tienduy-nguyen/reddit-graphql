import {
  Link as ChakraLink,
  Text,
  Code,
  List,
  ListIcon,
  ListItem,
  Box,
} from '@chakra-ui/react';
import { CheckCircleIcon, LinkIcon } from '@chakra-ui/icons';
import React from 'react';
import {
  ContainerFluidWrapper,
  CTA,
  Footer,
  Hero,
  Main,
} from 'src/components/Demo';
import { Container } from '@chakra-ui/react';
import { MetaTags } from 'src/components/MetaTags';
import { NextPage } from 'next';
import { withApollo } from 'src/utils/withApollo';
import { MainLayout } from 'src/components/MainLayout';

const HomePage: NextPage = () => (
  <MainLayout>
    <MetaTags title='Home' />
    <ContainerFluidWrapper>
      <Hero />
      <Main>
        <Container maxW='xl' mx='auto'>
          <Box>
            <Text>
              Server build with <Code>Node.js</Code> +{' '}
              <Code>Apollo GraphQL</Code> + <Code>PostgreSQL</Code> +{' '}
              <Code>TypeOrm</Code> +<Code>Typescript</Code>.
            </Text>
            <List spacing={3} my={3}>
              <ListItem>
                <ListIcon as={CheckCircleIcon} color='green.500' />
                <ChakraLink isExternal href='hhttps://nodejs.org/en/'>
                  Node.js <LinkIcon />
                </ChakraLink>
              </ListItem>
              <ListItem>
                <ListIcon as={CheckCircleIcon} color='green.500' />
                <ChakraLink isExternal href='https://www.apollographql.com'>
                  Apollo GraphQL <LinkIcon />
                </ChakraLink>
              </ListItem>
              <ListItem>
                <ListIcon as={CheckCircleIcon} color='green.500' />
                <ChakraLink isExternal href='https://typeorm.io/#/'>
                  TypeORM <LinkIcon />
                </ChakraLink>
              </ListItem>
            </List>
          </Box>

          <Box mt='4rem'>
            <Text>
              Client build with <Code>Next.js</Code> +{' '}
              <Code>Apollo GraphQL</Code> + <Code>Chakra-ui</Code>.
            </Text>
            <List spacing={3} my={3}>
              <ListItem mx='auto'>
                <ListIcon as={CheckCircleIcon} color='green.500' />
                <ChakraLink isExternal href='https://nextjs.org'>
                  Next.js <LinkIcon />
                </ChakraLink>
              </ListItem>
              <ListItem>
                <ListIcon as={CheckCircleIcon} color='green.500' />
                <ChakraLink isExternal href='https://chakra-ui.com'>
                  Chakra UI <LinkIcon />
                </ChakraLink>
              </ListItem>
            </List>
          </Box>
        </Container>
      </Main>

      <Footer>
        <Text>Next ❤️ GraphQL</Text>
      </Footer>
      <CTA />
    </ContainerFluidWrapper>
  </MainLayout>
);

// export default withUrqlClient(createUrqlClient)(Index);
export default HomePage;
// export default withApollo({ ssr: false })(HomePage);
