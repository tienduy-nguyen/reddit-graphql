import { yupResolver } from '@hookform/resolvers/yup';
import { useRouter } from 'next/router';
import React from 'react';
import * as yup from 'yup';
import { useForgotPasswordMutation } from 'src/generated/graphql';
import { useForm } from 'react-hook-form';
import { addServerErrors, toErrorMap } from 'src/utils';
import { MetaTags } from 'src/components/MetaTags';
import { BoxWrapper } from 'src/components/BoxWrapper';
import {
  Button,
  FormControl,
  FormErrorMessage,
  FormLabel,
  Input,
  Text,
  useToast,
} from '@chakra-ui/react';
import { NextPage } from 'next';
import { withApollo } from 'src/utils/withApollo';
import { MainLayout } from 'src/components/MainLayout';

type ForgotPasswordInputForm = {
  email: string;
};
const schema = yup.object().shape({
  email: yup.string().email().required(),
});

const ForgotPasswordPage: NextPage = ({}) => {
  const router = useRouter();
  const toast = useToast();
  const [forgotPassword] = useForgotPasswordMutation();
  const {
    register,
    handleSubmit,
    errors,
    setError,
  } = useForm<ForgotPasswordInputForm>({
    mode: 'onBlur',
    resolver: yupResolver(schema),
  });

  const onSubmit = async (values: ForgotPasswordInputForm) => {
    const response = await forgotPassword({
      variables: { email: values.email },
    });
    if (response?.data?.forgotPassword.sendEmail) {
      // worked
      toast({
        title: 'Send email',
        description: `Check your email to get link reset password`,
        status: 'success',
        duration: 2000,
        isClosable: true,
        position: 'top-right',
      });
      router.push('/');
      return;
    }

    if (response?.data?.forgotPassword.errors) {
      const serverErrors: Record<string, string> = toErrorMap(
        response?.data?.forgotPassword.errors
      );
      addServerErrors(serverErrors, setError);
    }
  };

  return (
    <MainLayout>
      <MetaTags title='Forgot password' />
      <BoxWrapper>
        <form style={{ width: 400 }}>
          <Text fontSize='2em' textAlign='center'>
            Forgot password
          </Text>
          <FormControl
            isInvalid={!!errors?.email?.message}
            errortext={errors?.email?.message}
            isRequired
            p='4'
            mt='4'
          >
            <FormLabel>Email</FormLabel>
            <Input
              type='email'
              name='email'
              placeholder='Enter your email'
              id='email'
              autoComplete='email'
              ref={register}
            />
            <FormErrorMessage>{errors?.email?.message}</FormErrorMessage>
          </FormControl>
          <Button
            mb='15'
            onClick={handleSubmit(onSubmit)}
            p='4'
            mx='4'
            mt='6'
            w='90%'
            colorScheme='green'
            type='submit'
          >
            Confirm
          </Button>
        </form>
      </BoxWrapper>
    </MainLayout>
  );
};

export default ForgotPasswordPage;
// export default withApollo({ ssr: false })(ForgotPasswordPage);
