import { FormControl, FormLabel } from '@chakra-ui/form-control';
import * as yup from 'yup';
import { yupResolver } from '@hookform/resolvers/yup';
import { useForm } from 'react-hook-form';
import { Input, InputGroup, InputRightElement } from '@chakra-ui/input';
import React from 'react';
import { Button, FormErrorMessage, Text, useToast } from '@chakra-ui/react';
import {
  MeDocument,
  MeQuery,
  useChangePasswordMutation,
} from 'src/generated/graphql';
import { useRouter } from 'next/router';
import { toErrorMap } from 'src/utils/toErrorMap';
import { addServerErrors } from 'src/utils/addServerError';
import { BoxWrapper } from 'src/components/BoxWrapper';
import { MetaTags } from 'src/components/MetaTags';
import { ViewIcon, ViewOffIcon } from '@chakra-ui/icons';
import { NextPage } from 'next';
import { withApollo } from 'src/utils/withApollo';
import { MainLayout } from 'src/components/MainLayout';

interface ChangePasswordProps {}

// Validation input form
const schema = yup.object().shape({
  oldPassword: yup.string().min(3).required(),
  newPassword: yup.string().min(3).required(),
});

// Input for register form
type ChangePasswordInputForm = {
  oldPassword: string;
  newPassword: string;
};

const ChangePasswordPage: NextPage<{ token: string }> = ({ token }) => {
  const router = useRouter();

  const toast = useToast();
  const [show, setShow] = React.useState(false);
  const handleClick = () => setShow(!show);

  const {
    register,
    handleSubmit,
    errors,
    setError,
  } = useForm<ChangePasswordInputForm>({
    mode: 'onBlur',
    resolver: yupResolver(schema),
  });

  // Get mutation graphql from mutation string
  const [changePasswordUser] = useChangePasswordMutation();

  // Submit form
  const onSubmit = async (values: ChangePasswordInputForm) => {
    const response = await changePasswordUser({
      variables: {
        token,
        oldPassword: values.oldPassword,
        newPassword: values.newPassword,
      },
      update: (cache, { data }) => {
        cache.writeQuery<MeQuery>({
          query: MeDocument,
          data: {
            __typename: 'Query',
            me: data?.changePassword.user,
          },
        });
      },
    });

    if (response?.data?.changePassword.errorToken) {
      toast({
        title: 'Token',
        description: response?.data?.changePassword.errorToken,
        status: 'error',
        duration: 4000,
        isClosable: true,
        position: 'top-right',
      });
      router.push('/');
      return;
    }

    if (response?.data?.changePassword.errors) {
      const serverErrors: Record<string, string> = toErrorMap(
        response?.data?.changePassword.errors
      );
      addServerErrors(serverErrors, setError);
    } else if (response?.data?.changePassword.user) {
      // worked
      toast({
        title: 'Reset password successfully!',
        status: 'success',
        duration: 2000,
        isClosable: true,
        position: 'top-right',
      });
      router.push('/');
    }
  };

  return (
    <MainLayout>
      <MetaTags title='Reset password' />
      <BoxWrapper>
        <form
          style={{
            width: 400,
          }}
        >
          <Text fontSize='2em' textAlign='center'>
            Reset password
          </Text>
          <Text hidden>{token}</Text>
          <FormControl
            isInvalid={!!errors?.oldPassword?.message}
            errortext={errors?.oldPassword?.message}
            isRequired
            p='4'
            mt='4'
          >
            <FormLabel>Old password</FormLabel>

            <InputGroup>
              <Input
                type={show ? 'text' : 'password'}
                name='oldPassword'
                placeholder='Password'
                id='oldPassword'
                autoComplete='oldPassword'
                ref={register}
              />
              <InputRightElement width='4.5rem'>
                <Button h='1.75rem' size='sm' onClick={handleClick}>
                  {show ? <ViewOffIcon /> : <ViewIcon />}
                </Button>
              </InputRightElement>
            </InputGroup>

            <FormErrorMessage>{errors?.oldPassword?.message}</FormErrorMessage>
          </FormControl>

          <FormControl
            isInvalid={!!errors?.newPassword?.message}
            errortext={errors?.newPassword?.message}
            px='4'
            pb='4'
            isRequired
          >
            <FormLabel>New password</FormLabel>
            <InputGroup>
              <Input
                type={show ? 'text' : 'password'}
                name='newPassword'
                placeholder='Password'
                id='newPassword'
                autoComplete='newPassword'
                ref={register}
              />
              <InputRightElement width='4.5rem'>
                <Button h='1.75rem' size='sm' onClick={handleClick}>
                  {show ? <ViewOffIcon /> : <ViewIcon />}
                </Button>
              </InputRightElement>
            </InputGroup>
            <FormErrorMessage>{errors?.newPassword?.message}</FormErrorMessage>
          </FormControl>

          <Button
            mb='15'
            onClick={handleSubmit(onSubmit)}
            p='4'
            mx='4'
            mt='6'
            w='90%'
            colorScheme='green'
            type='submit'
          >
            Submit
          </Button>
        </form>
      </BoxWrapper>
    </MainLayout>
  );
};

ChangePasswordPage.getInitialProps = ({ query }) => {
  return {
    token: query.token as string,
  };
};

// export default withUrqlClient(createUrqlClient)(Login);
export default ChangePasswordPage;
// export default withApollo({ ssr: false })(ChangePasswordPage);
