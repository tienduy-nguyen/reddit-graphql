import { useRouter } from 'next/router';
import { usePostQuery } from 'src/generated/graphql';

export const useGetPostFromUrl = () => {
  const router = useRouter();
  const id = typeof router.query.id === 'string' ? router.query.id : undefined;
  return usePostQuery({
    variables: {
      id: id,
    },
  });
};
