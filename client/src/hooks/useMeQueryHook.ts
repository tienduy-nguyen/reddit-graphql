import { useMeQuery } from '../generated/graphql';
import { useEffect, useState } from 'react';

export const useMeQueryHook = () => {
  const { data, loading } = useMeQuery();
  const [user, setUser] = useState(null);
  useEffect(() => {
    if (!loading && data?.me) {
      setUser(data.me);
    }
  }, [loading, data]);
  return [user, setUser];
};
