import nodemailer from 'nodemailer';

export interface EmailInput {
  from?: string;
  to: string;
  subject: string;
  html: string;
}
export async function sendEmail({
  from = '"Fred Foo 👻" <foo@example.com>',
  to,
  subject,
  html,
}: EmailInput) {
  const transporter = nodemailer.createTransport({
    host: 'smtp.ethereal.email',
    port: 587,
    secure: false, // true for 465, false for other ports
    auth: {
      user: 'mds43vi6nviwucqv@ethereal.email', // generated ethereal user
      pass: 'xJsQzVAuFYKqx5xUR9', // generated ethereal password
    },
  });

  // send mail with defined transport object
  const info = await transporter.sendMail({
    from: from, // sender address
    to: to, // list of receivers
    subject, // Subject line
    html,
  });

  console.log('Message sent: %s', info.messageId);
  console.log('Preview URL: %s', nodemailer.getTestMessageUrl(info));
}
