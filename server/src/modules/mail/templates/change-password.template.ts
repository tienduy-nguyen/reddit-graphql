export const changePasswordTemplate = (token: string) => {
  return `
  <div>
  
    <h3>Click to the link below to reset your password</h3>
    <a href="http://localhost:3000/change-password/${token}">
        http://localhost:3000/change-password/${token}
    </a>
  </div>
  `;
};
