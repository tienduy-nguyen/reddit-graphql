// m to n
// many to many
// user <-> posts
// user -> join table <- posts
// user -> updoot <- posts

import { Post } from '@modules/post/post.model';
import { User } from '@modules/user/user.model';
import { BaseEntity, Column, Entity, ManyToOne, PrimaryColumn } from 'typeorm';

@Entity('Updoot')
export class Updoot extends BaseEntity {
  @Column({ type: 'int' })
  value: number;

  @PrimaryColumn()
  userId: string;

  @ManyToOne(() => User, user => user.updoots)
  user: User;

  @PrimaryColumn()
  postId: string;

  @ManyToOne(() => Post, post => post.updoots, { onDelete: 'CASCADE' })
  post: Post;
}
