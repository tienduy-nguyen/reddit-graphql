import { capitalizeString } from '@common/utils/capitalize-string';
import { slugifyTitle } from '@common/utils/slugify-title';
import { Updoot } from '@modules/updoot/updoot.model';
import { User } from '@modules/user/user.model';
import { Field, ID, Int, ObjectType } from 'type-graphql';
import {
  BaseEntity,
  BeforeInsert,
  Column,
  CreateDateColumn,
  Entity,
  ManyToOne,
  OneToMany,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';

@ObjectType('Post')
@Entity('post')
export class Post extends BaseEntity {
  @Field(() => ID)
  @PrimaryGeneratedColumn('uuid')
  id!: string;

  @Field()
  @Column()
  title!: string;

  @Field()
  @Column()
  content!: string;

  @Field()
  @Column({ unique: true })
  slug!: string;

  @Field()
  @Column({ type: 'int', default: 0 })
  points!: number;

  @Field(() => Int, { nullable: true })
  voteStatus: number | null;

  @Field()
  @Column()
  authorId: string;

  @ManyToOne(() => User, user => user.posts)
  author: User;

  @OneToMany(() => Updoot, updoot => updoot.post)
  updoots: Updoot[];

  @Field(() => String)
  @CreateDateColumn()
  createdAt: Date;

  @Field(() => String)
  @UpdateDateColumn()
  updatedAt: Date;

  // Hook
  // Update auto slug when create new post
  @BeforeInsert()
  initSlug() {
    this.slug = slugifyTitle(this.title);
    this.title = capitalizeString(this.title);
  }
}
