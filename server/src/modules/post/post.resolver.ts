import { isAuth } from '@common/global-middlewares/auth.middleware';
import { IHttpContext } from '@common/global-types/http.interface';
import { User } from '@modules/user/user.model';
import {
  Arg,
  Ctx,
  FieldResolver,
  Mutation,
  Query,
  Resolver,
  Root,
  UseMiddleware,
  Int,
} from 'type-graphql';
import { getConnection } from 'typeorm';
import { CreatePostInput } from './dto/create-post.input';
import { UpdatePostInput } from './dto/update-post.input';
import { PaginatedPosts } from './object-types/paginated-posts';
import { Post } from './post.model';

@Resolver(() => Post)
export class PostResolver {
  @Query(() => PaginatedPosts)
  public async posts(
    @Arg('limit', () => Int) limit: number,
    @Arg('offset', () => Int, { nullable: true }) offset: number | null,
    @Arg('cursor', () => String, { nullable: true }) cursor: string | null,
  ) {
    // 20 -> 21
    const realLimit = Math.min(50, limit);
    const realLimitPlusOne = realLimit + 1;
    const replacements: any[] = [realLimitPlusOne];
    if (offset) {
      replacements.push(offset);
    }

    if (cursor) {
      replacements.push(new Date(parseInt(cursor)));
    }

    const posts = await getConnection().query(
      `
    select p.*
    from post p
    ${cursor ? `where p."createdAt" <= $3` : ''}
    order by p."createdAt" DESC
    ${offset ? `offset $2` : ''}
    limit $1
    `,
      replacements,
    );

    return {
      posts: posts.slice(0, realLimit),
      hasMore: posts.length === realLimitPlusOne,
    };
  }

  @FieldResolver(() => String)
  contentSnippet(@Root() post: Post) {
    return post.content.slice(0, 50) + ' ...';
  }

  @Query(() => Post)
  public async post(@Arg('id') id: string): Promise<Post | null> {
    return await Post.findOne(id);
  }

  @FieldResolver(() => User)
  author(@Root() post: Post, @Ctx() { userLoader }: IHttpContext) {
    return userLoader.load(post.authorId);
  }

  @Mutation(() => Post)
  @UseMiddleware(isAuth)
  public async createPost(
    @Arg('data') data: CreatePostInput,
    @Ctx() ctx: IHttpContext,
  ): Promise<Post | null> {
    const newPost = await Post.create({
      ...data,
      authorId: ctx.req.session.userId,
    }).save();
    return newPost;
  }

  @FieldResolver(() => String, { nullable: true })
  async voteStatus(
    @Root() post: Post,
    @Ctx() { updootLoader, req }: IHttpContext,
  ) {
    if (!req.session.userId) {
      return null;
    }

    const updoot = await updootLoader.load({
      postId: post.id,
      userId: req.session.userId,
    });

    return updoot ? updoot.value : null;
  }

  @Mutation(() => Post)
  @UseMiddleware(isAuth)
  public async updatePost(
    @Arg('id', () => String) id: string,
    @Arg('data') data: UpdatePostInput,
    @Ctx() ctx: IHttpContext,
  ): Promise<Post | null> {
    const { title, content } = data;
    const result = await getConnection()
      .createQueryBuilder()
      .update(Post)
      .set({ title, content })
      .where('id = :id and "authorId" = :authorId', {
        id,
        authorId: ctx.req.session.userId,
      })
      .returning('*')
      .execute();
    return result.raw[0];
  }

  @Mutation(() => Boolean)
  @UseMiddleware(isAuth)
  async vote(
    @Arg('value', () => Int) value: number,
    @Arg('postId', () => String) postId: string,
    @Ctx() { req }: IHttpContext,
  ) {
    const isUpdoot = value !== -1;
    const realValue = isUpdoot ? 1 : -1;
    const { userId } = req.session;

    // const updoot = await Updoot.findOne({ where: { postId, userId } });
    const updoot = await getConnection().query(
      `
    select up.*
    from updoot up
    where "postId" = '${postId}' and "userId" = '${userId}';
    `,
    );

    // the user has voted on the post before
    // and they are changing their vote
    if (updoot && updoot.value !== realValue) {
      await getConnection().transaction(async tm => {
        await tm.query(
          `
    update updoot
    set value = $1
    where "postId" = $2 and "userId" = $3
        `,
          [realValue, postId, userId],
        );

        await tm.query(
          `
          update post
          set points = points + $1
          where id = $2
        `,
          [2 * realValue, postId],
        );
      });
    } else if (!updoot) {
      // has never voted before
      await getConnection().transaction(async tm => {
        await tm.query(
          `
    insert into updoot ("userId", "postId", value)
    values ($1, $2, $3)
        `,
          [userId, postId, realValue],
        );

        await tm.query(
          `
    update post
    set points = points + $1
    where id = $2
      `,
          [realValue, postId],
        );
      });
    }
    return true;
  }

  @Mutation(() => Boolean)
  @UseMiddleware(isAuth)
  public async deletePost(
    @Arg('id', () => String) id: string,
    @Ctx() ctx: IHttpContext,
  ): Promise<boolean> {
    await Post.delete({ id: id, authorId: ctx.req.session.userId });
    return true;
  }
}
