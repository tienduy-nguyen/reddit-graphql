import { Field, InputType } from 'type-graphql';

@InputType()
export class PostWhereUniqueInput {
  @Field(() => String, { nullable: true })
  id?: string;

  @Field(() => String, { nullable: true })
  slug?: string;
}
