import { capitalizeString } from '@common/utils/capitalize-string';
import { slugifyTitle } from '@common/utils/slugify-title';
import {
  EntitySubscriberInterface,
  EventSubscriber,
  UpdateEvent,
} from 'typeorm';
import { Post } from './post.model';

@EventSubscriber()
export class PostEventSubscriber implements EntitySubscriberInterface<Post> {
  listenTo() {
    return Post;
  }

  async beforeUpdate({ entity, databaseEntity }: UpdateEvent<Post>) {
    if (entity.title !== databaseEntity.title) {
      entity.slug = slugifyTitle(entity.title);
      entity.title = capitalizeString(entity.title);
    }
  }
}
