import { Field, ObjectType } from 'type-graphql';
import { Post } from '../post.model';

@ObjectType()
export class PaginatedPosts {
  @Field(() => [Post])
  posts: Post[];

  @Field()
  hasMore: boolean;
}
