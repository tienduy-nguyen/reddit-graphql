import Redis from 'ioredis';

export const getRedisClient = (): Redis.Redis => {
  const redis = new Redis();
  return redis;
};
