import { IsEmail } from 'class-validator';
import { Field, InputType } from 'type-graphql';
// import { UserExistsValidator } from '../decorators/user-exists.decorator';

@InputType()
export class RegisterUserInput {
  @Field()
  // @Validate(UserExistsValidator)
  username!: string;

  @Field()
  @IsEmail()
  // @Validate(UserExistsValidator)
  email!: string;

  @Field()
  password!: string;
}
