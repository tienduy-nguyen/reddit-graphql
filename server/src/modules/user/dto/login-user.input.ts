import { IsNotEmpty, IsString } from 'class-validator';
import { Field, InputType } from 'type-graphql';

@InputType()
export class LoginUserInput {
  @Field()
  @IsString()
  @IsNotEmpty()
  usernameOrEmail?: string;

  @Field()
  @IsString()
  @IsNotEmpty()
  password!: string;
}
