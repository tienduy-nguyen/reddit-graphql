import { Field, ObjectType } from 'type-graphql';
import { User } from '../user.model';
import { FieldError } from './field-error';

@ObjectType()
export class UserResponse {
  @Field(() => [FieldError], { nullable: true })
  errors?: FieldError[];

  @Field(() => User, { nullable: true })
  user?: User;
}
