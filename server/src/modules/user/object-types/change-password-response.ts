import { Field, ObjectType } from 'type-graphql';
import { User } from '../user.model';
import { FieldError } from './field-error';

@ObjectType()
export class ChangePasswordResponse {
  @Field(() => String, { nullable: true })
  errorToken?: string;

  @Field(() => [FieldError], { nullable: true })
  errors?: FieldError[];

  @Field(() => User, { nullable: true })
  user?: User;
}
