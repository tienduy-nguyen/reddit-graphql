import { IHttpContext } from '@common/global-types/http.interface';
import {
  Arg,
  Ctx,
  FieldResolver,
  Mutation,
  Query,
  Resolver,
  Root,
  UseMiddleware,
} from 'type-graphql';
import { User } from './user.model';
import argon from 'argon2';
import { isAuth } from '@common/global-middlewares/auth.middleware';
import { v4 as uuid } from 'uuid';
import { EmailInput, sendEmail } from '@modules/mail/send-mail';
import { REDIS_FORGOT_PASSWORD_PREFIX } from '@modules/redis/redis.constants';
import { changePasswordTemplate } from '@modules/mail/templates/change-password.template';
import { ChangePasswordInput } from './dto';
import { ForgotPasswordResponse } from './object-types/forgot-password-response';
import { ChangePasswordResponse } from './object-types/change-password-response';

@Resolver(() => User)
export class UserResolver {
  @Query(() => User)
  @UseMiddleware(isAuth)
  public async me(@Ctx() ctx: IHttpContext) {
    const userId = ctx.req?.session?.userId;
    if (!userId) {
      return null;
    }
    return await User.findOne(userId);
  }

  @FieldResolver(() => String)
  email(@Root() user: User, @Ctx() { req }: IHttpContext) {
    // this is the current user and its ok to show them their own email
    if (req.session.userId === user.id) {
      return user.email;
    }
    // current user wants to see someone elses email
    return '';
  }

  @Query(() => [User])
  @UseMiddleware(isAuth)
  public async users() {
    return await User.find({});
  }

  @Mutation(() => ForgotPasswordResponse)
  public async forgotPassword(
    @Arg('email') email: string,
    @Ctx() { redis }: IHttpContext,
  ) {
    try {
      const user = await User.findOne({ where: { email } });
      if (!user) {
        return {
          errors: [
            {
              field: 'email',
              message: `No user found with email: ${email}`,
            },
          ],
        };
      }

      // Update redis
      const token = uuid();

      await redis.set(
        REDIS_FORGOT_PASSWORD_PREFIX + token,
        user.id,
        'ex',
        1000 * 60 * 60 * 24 * 1,
      ); // 1day

      const emailInput: EmailInput = {
        to: user.email,
        subject: 'Reset password',
        html: changePasswordTemplate(token),
      };

      // Send mail
      sendEmail(emailInput);
      return {
        sendEmail: true,
      };
    } catch (error) {
      return {
        errors: [
          {
            field: 'email',
            message: error.message,
          },
        ],
      };
    }
  }

  @Mutation(() => ChangePasswordResponse)
  public async changePassword(
    @Arg('data') { token, oldPassword, newPassword }: ChangePasswordInput,
    @Ctx() { req, redis }: IHttpContext,
  ): Promise<ChangePasswordResponse> {
    const userId = await redis.get(REDIS_FORGOT_PASSWORD_PREFIX + token);
    if (!userId) {
      return {
        errorToken: 'Token is expired or not valid.',
      };
    }
    const user = await User.findOne(userId);
    if (!user) {
      return {
        errorToken: 'Token is not valid.',
      };
    }
    const isMatch = await argon.verify(user.password, oldPassword);
    if (!isMatch) {
      return {
        errors: [
          {
            field: 'oldPassword',
            message: 'Old password not match',
          },
        ],
      };
    }
    user.password = await argon.hash(newPassword);
    const updated = await User.save(user);
    req.session.userId = updated.id;

    // Delete token key in redis
    redis.del(REDIS_FORGOT_PASSWORD_PREFIX + token);
    return { user: updated };
  }
}
