import { IHttpContext } from '@common/global-types/http.interface';
import { Arg, Ctx, Mutation, Resolver, UseMiddleware } from 'type-graphql';
import { LoginUserInput, RegisterUserInput } from './dto';
import { User } from './user.model';
import argon from 'argon2';
import { isAuth } from '@common/global-middlewares/auth.middleware';
import { UserResponse } from './object-types/user-response';
import { emailRegex } from '@common/utils/regex-pattern';

@Resolver(() => User)
export class AuthResolver {
  @Mutation(() => UserResponse)
  public async login(
    @Arg('data') data: LoginUserInput,
    @Ctx() ctx: IHttpContext,
  ): Promise<UserResponse> {
    try {
      const { usernameOrEmail, password } = data;
      let user: User;
      if (emailRegex.test(usernameOrEmail)) {
        // Using email
        user = await User.findOne({ where: { email: usernameOrEmail } });
      } else {
        // Using username
        user = await User.findOne({ where: { username: usernameOrEmail } });
      }
      if (!user) {
        return {
          errors: [
            { field: 'username', message: `Invalid credentials` },
            { field: 'username', message: `Invalid credentials` },
          ],
        };
      }
      const isMatch = await argon.verify(user.password, password);
      if (!isMatch) {
        return {
          errors: [
            { field: 'password', message: `Invalid credentials` },
            { field: 'username', message: `Invalid credentials` },
          ],
        };
      }

      ctx.req.session.userId = user.id;
      return { user };
    } catch (error) {
      return {
        errors: [{ field: '', message: `Something go wrong!` }],
      };
    }
  }

  @Mutation(() => UserResponse)
  public async register(
    @Arg('data') data: RegisterUserInput,
    @Ctx() ctx: IHttpContext,
  ): Promise<UserResponse> {
    // Check username if it exists already?
    const { username, email, password } = data;
    let check = await User.findOne({ where: { username } });
    if (check) {
      return {
        errors: [
          {
            field: 'username',
            message: `${username} already exists`,
          },
        ],
      };
    }

    // Check email if exists already?
    check = await User.findOne({ where: { email } });
    if (check) {
      return {
        errors: [{ field: 'email', message: `${email} already exists` }],
      };
    }
    const user = User.create({ ...data });
    user.password = await argon.hash(password);
    await User.save(user);

    ctx.req.session.userId = user.id;
    return { user };
  }

  @Mutation(() => Boolean)
  @UseMiddleware(isAuth)
  public async logout(@Ctx() ctx: IHttpContext) {
    try {
      ctx.req.res?.clearCookie('SESSION_AUTH');
      await ctx.req.session?.destroy();
      return true;
    } catch (error) {
      console.log(error);
      return false;
    }
  }
}
