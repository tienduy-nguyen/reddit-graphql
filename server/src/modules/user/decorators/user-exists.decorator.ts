import {
  ValidationArguments,
  ValidatorConstraint,
  ValidatorConstraintInterface,
} from 'class-validator';
import { User } from '../user.model';

@ValidatorConstraint({ name: 'user', async: true })
export class UserExistsValidator implements ValidatorConstraintInterface {
  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  public async validate(emailOrUsername: string, _args: ValidationArguments) {
    let user = await User.findOne({ where: { email: emailOrUsername } });
    if (user) return false;

    user = await User.findOne({ where: { username: emailOrUsername } });
    if (user) return false;

    return true;
  }
  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  defaultMessage(_args: ValidationArguments) {
    return 'User with $property $value already exists';
  }
}
