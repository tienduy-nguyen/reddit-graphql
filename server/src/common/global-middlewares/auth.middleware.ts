import { BadRequestException } from '@common/exceptions';
import { IHttpContext } from '@common/global-types/http.interface';
import { MiddlewareFn } from 'type-graphql';

export const isAuth: MiddlewareFn<IHttpContext> = async ({ context }, next) => {
  if (!context.req.session.userId) {
    throw new BadRequestException('Not authenticated');
  }
  return next();
};
