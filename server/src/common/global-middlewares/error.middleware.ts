import { HttpException } from '@common/exceptions';
import { Request, Response } from 'express';

export const errorMiddleware = (
  err: HttpException,
  req: Request,
  res: Response,
) => {
  res.status(err.status || 500);
  res.send({
    errors: {
      message: err.message || 'Inferrer error server!',
    },
  });
};
