import { createUpdootLoader } from '@common/utils/createUpdootLoader';
import { createUserLoader } from '@common/utils/createUserLoader';
import { User } from '@modules/user/user.model';
import { Response } from 'express';
import { Redis } from 'ioredis';

export interface IHttpContext {
  req?: IRequestWithUser;
  res?: Response;
  redis?: Redis;
  userLoader?: ReturnType<typeof createUserLoader>;
  updootLoader?: ReturnType<typeof createUpdootLoader>;
}
export interface IRequestWithUser {
  session?: any;
  user?: User;
  res?: Response;
}

export interface IUserFromRequest {
  id: string;
  email: string;
  username?: string;
}
