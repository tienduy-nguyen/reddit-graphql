import { getRedisClient } from '@modules/redis/redis-client';
import connectRedis from 'connect-redis';
import session from 'express-session';

export function sessionConfig() {
  const RedisStore = connectRedis(session);
  const redisClient = getRedisClient();
  const __prod__ = process.env.NODE_ENV === 'production';
  return {
    store: new RedisStore({ client: redisClient as any, disableTouch: true }),
    name: 'SESSION_AUTH',
    secret: process.env.SESSION_SECRET,
    resave: false,
    saveUninitialized: false,
    cookie: {
      httpOnly: true,
      secure: process.env.NODE_ENV === 'production',
      maxAge: 1000 * 60 * 60 * 24 * 30, // 30 days,
      sameSite: 'lax', // csrf
      domain: __prod__ ? '.codeponder.com' : undefined,
    },
  };
}
