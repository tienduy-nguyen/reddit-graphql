import dotenv from 'dotenv';

export const envConfig = () => {
  dotenv.config({ path: process.cwd() + '/.env.production' });
  return {
    isProd: process.env.NODE_ENV === 'production',
  };
};
