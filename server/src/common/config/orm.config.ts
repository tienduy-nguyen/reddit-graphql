export function ormConfig(): any {
  return {
    type: 'postgres',
    name: 'default',
    host: 'localhost',
    port: 5432,
    username: 'postgres',
    password: 'postgres',
    database: 'reddit_db',
    entities: ['src/modules/**/*.model.ts'],
    subscribers: ['src/modules/**/*.subscriber.ts'],
    logging: false,
    synchronize: true,
    migrations: ['src/common/migrations/**/*.ts'],
    cli: {
      migrationsDir: 'src/common/migrations',
    },
  };
}
