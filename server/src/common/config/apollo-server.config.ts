import { appSchema } from '@app/app.schema';
import { createUpdootLoader } from '@common/utils/createUpdootLoader';
import { createUserLoader } from '@common/utils/createUserLoader';
import { getRedisClient } from '@modules/redis/redis-client';

export const apolloServerConfig = async () => {
  const schema = await appSchema();

  return {
    schema,
    context: ({ req, res }: any) => ({
      req,
      res,
      redis: getRedisClient(),
      userLoader: createUserLoader(),
      updootLoader: createUpdootLoader(),
    }),
  };
};
