import { randomBytes } from 'crypto';

export function randomString(length: number): string {
  return randomBytes(256).toString('hex').substring(0, length);
}
