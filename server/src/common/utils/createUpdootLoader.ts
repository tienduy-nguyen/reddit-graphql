import { Updoot } from '@modules/updoot/updoot.model';
import DataLoader from 'dataloader';

export const createUpdootLoader = () =>
  new DataLoader<{ postId: string; userId: string }, Updoot | null>(
    async keys => {
      const updoots = await Updoot.findByIds(keys as any);
      const updootIdsToUpdoot: Record<string, Updoot> = {};
      updoots.forEach(updoot => {
        updootIdsToUpdoot[`${updoot.userId}|${updoot.postId}`] = updoot;
      });

      return keys.map(key => updootIdsToUpdoot[`${key.userId}|${key.postId}`]);
    },
  );
