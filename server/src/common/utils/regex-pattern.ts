export const emailRegex = /^[\w-\.]+@([\w-]+\.)+[\w-]{2,4}$/;
export const passwordRegex = /^(?=.*\d)(?=.*[a-zA-Z]).{6,}$/;
export const usernameRegex = /.{3,35}/;
