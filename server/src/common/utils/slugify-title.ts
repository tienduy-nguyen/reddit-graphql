import { randomString } from './random-string';
import slugify from 'slugify';

export function slugifyTitle(title: string, randomStringLength = 6) {
  const suffix = randomString(randomStringLength);
  const titleKebabCase = slugify(title, { lower: true });
  return `${titleKebabCase}-${suffix}`;
}
