declare namespace NodeJS {
  export interface ProcessEnv {
    NODE_ENV: string;
    PORT: string;
    SITE_URL: string;
    CORS_ORIGIN: string;
    DB_CONNECTOR: string;
    DB_HOST: string;
    DB_USER: string;
    DB_PASSWORD: string;
    DB_DATABASE: string;
    DB_PORT: string;
    DATABASE_URL: string;
    REDIS_URL: string;
    SESSION_SECRET: string;
  }
}
