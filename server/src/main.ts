import 'reflect-metadata';
import { ApolloServer } from 'apollo-server-express';
import express, { Application } from 'express';
import { createConnection } from 'typeorm';
import dotenv from 'dotenv';
import { errorMiddleware } from '@common/global-middlewares';
import { apolloServerConfig, ormConfig } from '@common/config';
import { sessionConfig } from '@common/config/session.config';
import session from 'express-session';
import cookieParser from 'cookie-parser';
import cors from 'cors';
import helmet from 'helmet';
import compression from 'compression';
import RateLimit from 'express-rate-limit';

const bootstrap = async () => {
  dotenv.config();

  // Typeorm connection
  try {
    await createConnection(ormConfig());
    // await conn.runMigrations();
  } catch (error) {
    console.error(error.message);
    return error;
  }

  const port = Number(process.env.PORT) || 4220;
  const apolloServer = new ApolloServer(await apolloServerConfig());

  const app: Application = express();
  app.set('trust proxy', 1);
  app.use(express.json());
  app.use(express.urlencoded({ extended: true }));
  app.use(
    cors({
      origin: 'http://localhost:3000',
      credentials: true,
    }),
  );
  app.use(cookieParser());

  if (process.env.NODE_ENV === 'production') {
    app.enable('trust proxy'); // only if you're behind a reverse proxy (Heroku, Bluemix, AWS ELB, Nginx, etc)
    app.use(compression());
    app.use(helmet());

    app.use(
      RateLimit({
        windowMs: 15 * 60 * 1000, // 15 minutes
        max: 100, // limit each IP to 100 requests per windowMs
      }),
    );
  }

  // Setup session with redis
  const sessionOptions = sessionConfig();
  app.use(session(sessionOptions));

  // Apply apollo server to app
  apolloServer.applyMiddleware({ app, cors: false });

  // Handle error
  app.use(errorMiddleware);

  app.listen(port, () => {
    console.log(
      `Server graphql is running at http://localhost:${port}/graphql`,
    );
  });
};

bootstrap().catch(err => {
  console.error(err);
});
