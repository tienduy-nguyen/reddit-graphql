import { PostResolver } from '@modules/post/post.resolver';
import { AuthResolver } from '@modules/user/auth.resolver';
import { UserResolver } from '@modules/user/user.resolver';
import { buildSchema } from 'type-graphql';
import { AppResolver } from './app.resolver';

export const appSchema = async () => {
  const schemas = await buildSchema({
    resolvers: [AppResolver, AuthResolver, UserResolver, PostResolver],
    validate: false,
  });
  return Promise.resolve(schemas);
};
