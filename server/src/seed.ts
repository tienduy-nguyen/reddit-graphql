import 'reflect-metadata';
import { ormConfig } from './common/config';
import { Post } from './modules/post/post.model';
import { Updoot } from './modules/updoot/updoot.model';
import { User } from './modules/user/user.model';
import { createConnection } from 'typeorm';
import faker from 'faker';
import argon from 'argon2';

let connection;

async function main() {
  console.log('Start seeding ................................................');
  connection = await createConnection(ormConfig());

  // Clear data
  connection.createQueryBuilder().delete().from(User).execute();
  connection.createQueryBuilder().delete().from(Post).execute();
  connection.createQueryBuilder().delete().from(Updoot).execute();
  // .where('entity.id IN (:...ids)', { ids: entityIds })

  // User seeding
  const user1 = await User.create({
    username: 'user1',
    email: 'user1@yopmail.com',
    password: await argon.hash('1234567'),
  }).save();

  // Post seeding

  for (let num = 0; num < 10; num++) {
    const content = `${faker.hacker.phrase()}
    ${faker.finance.transactionDescription()}
    ${faker.lorem.paragraphs()}
    `;

    let title = faker.hacker.phrase();
    if (title.length > 50) {
      title = title.replace(/^(.{10}[^\s]*).*/, '$1');
    }
    await Post.create({
      title: title,
      content: content,
      authorId: user1.id,
    }).save();
  }

  console.log('Create user: ', user1);
  console.log('And 10 posts created');
}

main()
  .catch(err => {
    console.log(err);
  })
  .finally(async () => {
    await connection.close();
    console.log(
      'End seeding .................................................',
    );
  });
